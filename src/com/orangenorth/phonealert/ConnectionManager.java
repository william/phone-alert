package com.orangenorth.phonealert;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

public class ConnectionManager {
    private final List<BluetoothSocket> clientSockets;
    private final List<ConnectionReader> readers;
    private final ConnectionWriter writer;
    private final byte[] handshake;

    ConnectionManager() {
        clientSockets = new LinkedList<BluetoothSocket>();
        readers = new LinkedList<ConnectionReader>();
        writer = new ConnectionWriter(this);
        writer.start();
        String handshakeString = "BluetoothPhoneNotifications Version: 1";
        handshake = handshakeString.getBytes(Charset.forName("UTF-8"));
    }

    synchronized private void addClient(BluetoothSocket client) {
        clientSockets.add(client);
    }

    synchronized private void removeClient(BluetoothSocket client) {
        clientSockets.remove(client);
    }

    synchronized private List<BluetoothSocket> getClientListCopy() {
        List<BluetoothSocket> clientsCopy = new LinkedList<BluetoothSocket>();
        clientsCopy.addAll(clientSockets);
        return clientsCopy;
    }

    public void manageConnectedSocket(BluetoothSocket socket) {
        try {
            ConnectionReader reader = new ConnectionReader(socket, this);
            readers.add(reader);
            reader.start();
        } catch (IOException e) {
            // Bad connection, do nothing.
        }
    }

    public void shutdown() {
        for (ConnectionReader reader : readers) {
            reader.shutdown();
        }

        writer.shutdown();
    }

    public void sendPhoneNumber(String incomingNumber) {
        byte[] numberBytes = incomingNumber.getBytes(Charset.forName("UTF-8"));
        byte[] dataBytes = new byte[numberBytes.length + 2];
        dataBytes[0] = 1;
        dataBytes[1] = (byte) numberBytes.length;
        System.arraycopy(numberBytes, 0, dataBytes, 2, numberBytes.length);
        writer.sendData(dataBytes);
        Log.i("PhoneAlert", "Phone number prepared to be sent.");
    }

    private class ConnectionWriter extends Thread {
        private final ConnectionManager connectionManager;
        private final SynchronousQueue<byte[]> outboundData;

        ConnectionWriter(ConnectionManager connectionManager) {
            this.connectionManager = connectionManager;
            outboundData = new SynchronousQueue<byte[]>();
        }

        @Override
        public void run() {
            try {
                Log.i("PhoneAlert", "Writer started up and prepared to send messages.");
                while (true) {
                    byte[] writeData = outboundData.take();
                    Log.i("PhoneAlert", "Writer sending message.");
                    List<BluetoothSocket> clients = connectionManager.getClientListCopy();
                    for (BluetoothSocket client : clients) {
                        try {
                            Log.i("PhoneAlert", "Writer sending message to " +
                                    client.getRemoteDevice().getName());
                            OutputStream outputStream = client.getOutputStream();
                            outputStream.write(writeData);
                            outputStream.flush();
                        } catch (IOException e) {
                            // Something went wrong with the client.
                            connectionManager.removeClient(client);
                        }
                    }
                }
            } catch (InterruptedException ex) {
                // We are done here.
            }
            Log.i("PhoneAlert", "Writer shutting down.");
        }

        public void shutdown() {
            for (BluetoothSocket socket : clientSockets) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // Can't do anything about this here.
                }
            }

            interrupt();
        }

        public void sendData(byte[] bytes) {
            outboundData.add(bytes);
        }
    }

    private class ConnectionReader extends Thread {
        private final BluetoothSocket socket;
        private final ConnectionManager connectionManager;
        private final InputStream socketInput;

        ConnectionReader(BluetoothSocket socket, ConnectionManager connectionManager)
                throws IOException {
            this.socket = socket;
            this.connectionManager = connectionManager;
            socketInput = socket.getInputStream();
        }

        @Override
        public void run() {
            try {
                // Initiate handshake.
                OutputStream outputStream = socket.getOutputStream();
                outputStream.write(handshake);
                outputStream.flush();

                byte[] handshakeReceiver = new byte[handshake.length];
                socketInput.read(handshakeReceiver);

                if (Arrays.equals(handshakeReceiver, handshake)) {
                    connectionManager.addClient(socket);
                }

                while (true) {
                    int command = socketInput.read();
                    dispatch(command, socketInput);
                }
            } catch (IOException e) {
                // Something is stopping us from reading. We are done.
            }
        }

        private void dispatch(int command, InputStream socketInput) {
            switch (command) {
                case 0:
                    // Keep-alive. Do nothing.
                    Log.i("PhoneAlert", "Keep-Alive received.");
                    break;
            }
        }

        public void shutdown() {
            try {
                socketInput.close();
            } catch (IOException e) {
                // Can't recover. Do nothing.
            }

            try {
                socket.close();
            } catch (IOException e) {
                // Can't do anything here.
            }
        }
    }
}
