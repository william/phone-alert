package com.orangenorth.phonealert;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;

public class CommunicatorService extends Service {
    public static final int MSG_SEND_FAKE_CALL = 4;

    private final Messenger messenger;
    private BluetoothServer server;
    private final ConnectionManager connectionManager;
    private final PhoneListener phoneListener;

    public CommunicatorService() {
        messenger = new Messenger(new IncomingHandler());
        connectionManager = new ConnectionManager();
        phoneListener = new PhoneListener(connectionManager);
    }

    @Override
    public void onCreate() {
        TelephonyManager telephonyManager =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        Log.i("PhoneAlert", "Call listener attached.");

        try {
            server = new BluetoothServer(connectionManager);
            server.start();
        } catch (IOException e) {
            // Something bad happened. Nothing to do here.
        }
    }

    @Override
    public void onDestroy() {
        TelephonyManager telephonyManager =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_NONE);
        Log.i("PhoneAlert", "Call listener detached.");

        // Shutdown the server before the connection manager so
        // we don't try to use a dead connection manager.
        if (server != null) {
            server.shutdown();
        }

        if (connectionManager != null) {
            connectionManager.shutdown();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SEND_FAKE_CALL:
                    connectionManager.sendPhoneNumber("415-623-6294");
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
