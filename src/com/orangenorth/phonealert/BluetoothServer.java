package com.orangenorth.phonealert;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.UUID;

class BluetoothServer extends Thread {
    private BluetoothServerSocket serverSocket;
    private final ConnectionManager connectionManager;

    public BluetoothServer(ConnectionManager connectionManager) throws IOException {
        super("Bluetooth Server Thread");
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        UUID appUid = UUID.fromString("fd8b01e9-691e-4f78-bc74-ea185209239d");
        this.connectionManager = connectionManager;
        serverSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(
                "BluetoothPhoneNotifications", appUid);
    }

    public void run() {
        BluetoothSocket socket;
        while (true) {
            try {
                socket = serverSocket.accept();
                // If a connection was accepted
                if (socket != null) {
                    connectionManager.manageConnectedSocket(socket);
                }
            } catch (IOException e) {
                // We are no longer able to accept connections. Time to shut down.
                break;
            }
        }

        try {
            serverSocket.close();
        } catch (IOException e) {
            // Can't recover, don't do anything.
        }
    }

    public void shutdown() {
        try {
            connectionManager.shutdown();
            serverSocket.close();
        } catch (IOException e) {
            // Can't recover, don't do anything.
        }
    }
}