package com.orangenorth.phonealert;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

class PhoneListener extends PhoneStateListener {
    private final ConnectionManager connectionManager;

    public PhoneListener(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                Log.i("PhoneAlert", "Incoming call.");
                connectionManager.sendPhoneNumber(incomingNumber);
                break;
        }
    }
}
