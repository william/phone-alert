package com.orangenorth.phonealert;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.*;
import android.view.View;
import android.widget.Switch;

public class MainActivity extends Activity {
    private Messenger messenger;
    private final CommunicatorConnection communicatorConnection;

    public MainActivity() {
        messenger = new Messenger(new IncomingHandler());
        communicatorConnection = new CommunicatorConnection();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindIntent = new Intent(this, CommunicatorService.class);
        startService(bindIntent);
        bindService(bindIntent, communicatorConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(communicatorConnection);
        super.onStop();
    }

    public void setDiscoverable(View view) {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);
    }

    public void sendFakeCall(View view) {
        try {
            Message message = Message.obtain(null, CommunicatorService.MSG_SEND_FAKE_CALL);
            messenger.send(message);
        } catch (RemoteException e) {
            // Problem.
        }
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private class CommunicatorConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            messenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            messenger = null;
        }
    }
}
